<?php
declare(strict_types=1);

$configurator = new Nette\Configurator;
//$configurator->setDebugMode('8.8.8.8'); // enable for your remote IP
$configurator->enableTracy(BASE_PATH . '/log');
$configurator->setTimeZone('Europe/Prague');
$configurator->setTempDirectory(BASE_PATH . '/temp');
$configurator->addConfig(BASE_PATH . '/config/config.neon');

if (file_exists(BASE_PATH.'/.env')) {
    $loader = new josegonzalez\Dotenv\Loader(BASE_PATH.'/.env');
    $loader->parse();
    $environment = $loader->toArray();
    $configurator->addParameters($environment);
    $tracyDebug = $environment['TRACY_DEBUG'] ?? false;
    $configurator->setDebugMode($tracyDebug);
}

$container = $configurator->createContainer();
return $container;