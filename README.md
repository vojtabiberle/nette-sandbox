# Nette sandbox - my way

This is example how I think, Nette application structure should look like.

Do you ask why?

## Directory structure

 - `/config` - should be visible first and contain all application configuration
 - `/modules` - contain more application modules PSR-4 structured
 - `/public` - better name than www - public means something accessible from outside but www means world wide web
 
## Why not robo-loader
We have great composer loader and PSR-4.

## Why different `public/index.php`
Better sandboxing of application environment. Easier to understand. Better support for PHP standalone webserver runned by `php -S`.

## Why `bootstrap.php` in project root
Because project root is place, where common files of application should live.

## Use `.env` instead of `config.local.neon`
Is here because `.env` file is visible at root of project and configuration variables can be better structured and commented. Is not necessary to understand, how and why you can overide `config.neon` with different configuration.