<?php
declare(strict_types=1);

// Delegate static file requests back to the PHP built-in webserver
if (PHP_SAPI === 'cli-server' && $_SERVER['SCRIPT_FILENAME'] !== __FILE__) {
    return false;
}

define('BASE_PATH', dirname(__DIR__));

require BASE_PATH.'/vendor/autoload.php';

/**
 * Self-called anonymous function that creates its own scope and keep the global namespace clean.
 */
(function () {
    $container = require BASE_PATH . '/bootstrap.php';
    $container->getByType(Nette\Application\Application::class)
        ->run();
})();